import errno
import xml.etree.ElementTree as ET
import sys
import os
import re
import subprocess
from itertools import islice

"""
Author: Khoa Le
Version: Nov 8, 2018
"""
cucumber_feature = False
cucumber_failure = False

def main():
    directory, report_file_dir = gather_params()
    verify_inputs(directory, report_file_dir)
    while (True):
        wait_for_next_action(report_file_dir)

def wait_for_next_action(report_file_dir):

    cprint("\nActions:", bcolors.OKGREEN)
    print("c:    comment out failed test")
    print("r:    revert commented out tests back to original")
    print("s:    show failed test(s), if any")
    print("q:    quit")
    print("__________________________________")
    action_selected = ""
    version = sys.version[0:1]
    if int(version) > 2:
        action_selected = input()
    else:
        action_selected = raw_input()

    if action_selected == "q":
        exit()
    elif action_selected == "c":
        print("\nLooking for failed test(s) in {0}:".format(report_file_dir))
        traverse_dir_to_find_tests(report_file_dir, "comment-out")

    elif action_selected == "r":
        cprint("\nReverted commented-out tests to original" , bcolors.OKGREEN)
        traverse_dir_to_find_tests(report_file_dir, "revert-back")

    elif action_selected == "s":
        failed_tests = gather_failed_tests(report_file_dir, False)
        if len(failed_tests) > 0:
            cprint("Failed tests" if len(failed_tests) > 1 else "Failed test", bcolors.FAIL)
            for test in failed_tests:
                cprint(test, bcolors.WARNING)
            print("__________________________________")
        else:
            cprint("\nNo failed test found", bcolors.FAIL)

    else:
        cprint("Error: command not found", bcolors.FAIL)

"""
Verifies if the current project name is the same as
the xml test report's

Args:
    dir: the current project directory
    report_file_dir: xml test report file
"""
def verify_inputs(dir, report_file_dir):
    try:
        f = open (report_file_dir, "r")
        lines = f.readlines()
        project_report = ""
        for l in lines:
            if '<?xml' in l and l.index('<?xml') == 0:
                if 'project="' in l:
                    project_report = l[l.index('project="') + len('project="'): l.index('tests="') - 2]
                break
            elif '<testrun' in l and l.index('<testrun') == 0:
                if 'project="' in l:
                    project_report = l[l.index('project="') + len('project="'): l.index('started="') - 2]
                break
    except FileNotFoundError:
        cprint("ERROR: Cannot find xml test report file", bcolors.FAIL)
        cprint("Verify that report file name is correct as name might contain white space (use '\\' to separate white space)", bcolors.FAIL)
        quit()

    try:
        current_dir = os.getcwd()
        with open (current_dir + "/pom.xml") as pom:
            abridged = list(islice(pom, 7))
            name = abridged[6]
            current_project_dir = name[name.index("<name>") + 6 : name.index("</name>")]
            cprint("\nCurrent project directory: " + current_dir, bcolors.WARNING)
            cprint("Project's name from xml file: " + project_report, bcolors.WARNING)

            if current_project_dir != project_report:
                cprint("WARNING: The test report xml file " + project_report + " does not match the name of the current project directory " + current_project_dir, bcolors.WARNING)
    except FileNotFoundError:
        cprint("ERROR: This directory is not a Maven project containing pom.xml file.", bcolors.FAIL)
        quit()

"""
Find all the failed tests in the xml test report
Args:
    report_file_dir: xml test report file
"""
def traverse_dir_to_find_tests(report_file_dir, action):
    exclude = set([".git", "target", "resources", ".settings", "site", "changes"])

    # uncomment or comment out failed tests action
    if action == "revert-back":
        failed_tests = gather_failed_tests(report_file_dir, False)
    else:
        failed_tests = gather_failed_tests(report_file_dir, True)

    for root, dirs, files in os.walk("."):
        dirs[:] = [d for d in dirs if d not in exclude]
        paths = root.split(os.sep)
        current_path = '/'.join(paths)
        for file in files:
            if file.endswith("Test.java"):
                java_test_file_dir = current_path + "/" + file
                if action == "comment-out":
                    comment_out_all_tests(java_test_file_dir)
                    uncomment_out_failed_tests(java_test_file_dir, failed_tests)
                elif action == "revert-back":
                    uncomment_out_all_tests(java_test_file_dir)

def comment_out_all_tests(file):
    context = ""
    with open (file, "rt") as fin:
        context = fin.read().replace('public void test', 'public void _test')
    with open (file, "wt") as fout:
        fout.write(context)

def uncomment_out_all_tests(file):
    context = ""
    with open (file, "rt") as fin:
        context = fin.read().replace('public void _test', 'public void test')
    with open (file, "wt") as fout:
        fout.write(context)

def uncomment_out_failed_tests(file, failed_tests):
    context = []
    with open (file, "rt") as fin:
        lines = fin.readlines()
        for l in lines:
            for test in failed_tests:
                if 'public void _' + test + '()' in l:
                    l = l.replace('public void _' + test + '()', 'public void ' + test + '()')
                    context.append(l)
            context.append(l)

        for i in range (0, len(context) - 2):
            cur = context[i]
            next = context[i+1]
            if cur == next:
                context[i+1] = ''

    with open (file, "wt") as fout:
        fout.write(''.join(context))

def gather_params():
    if len(sys.argv) == 2:
        return (sys.argv[1], "")
    if len(sys.argv) == 3:
        return (sys.argv[1], sys.argv[2])
    return ("./", "")

'''
Gathers failed tests from the xml report
Args:
    initial_result file content when open
    inform: print the failede tests if true
'''
def gather_failed_tests(initial_result, inform):
    failed_tests = []
    f = open(initial_result, "r")
    if f.mode == 'r':
        lines = f.readlines()
        failed_tests = extract_failed_tests(lines)
    if len(failed_tests) > 0:
        if inform:
            cprint("Found " + str(len(failed_tests)) + " failed tests" if len(failed_tests) > 1 else "Found " + str(len(failed_tests)) + " failed test", bcolors.WARNING)
        for t in failed_tests:
            if inform:
                cprint(t, bcolors.FAIL)

        if inform:
            cprint("Isolated " + str(len(failed_tests)) + " failed tests for re-run" if len(failed_tests) > 1 else "Isolated " + str(len(failed_tests)) + " test for re-run", bcolors.OKGREEN)
            cprint("WARNING: Failed cucumber feature tests might not be filtered.", bcolors.WARNING)
            if cucumber_feature:
                cprint("Notice: Test project contains Cucumber feature", bcolors.WARNING)
            elif cucumber_feature and cucumber_failure:
                cprint("WARNING: One or more Cucumber features fail", bcolors.WARNING)
    else:
            if inform:
                cprint("No failed test found in test report", bcolors.FAIL)
            quit()
    return failed_tests

'''
Extract failed test info from the lines array
Args:
    lines array of lines
'''
def extract_failed_tests(lines):
    failed_tests = []
    for i in range (0, len(lines) - 1):
        this_line = lines[i]
        next_line = lines[i+1]

        if "testcase" not in this_line: continue
        if "</error>" in this_line : continue
        # Focus on MSVC-Reg junit test patterns
        if "testcase" in this_line and 'name="test' in this_line:
            if "error" in next_line or "failure" in next_line:
                if this_line.index("name") < this_line.index("classname"):
                    failed_tests.append(this_line[this_line.index('"test') + 1 : this_line.index('" classname')])
                else:
                    failed_tests.append(this_line[this_line.index('"test') + 1 : this_line.index('" time')])
        # Cucumber feature check
        if "feature" in this_line and "failure" in next_line:
            cucumber_feature = True
            cucumber_failure = True
        elif "feature" in this_line and "failure" not in next_line:
            cucumber_feature = True
    return failed_tests;

"""
Author: Antonio Woods
"""
class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

def cprint(message, color):
    print("{0}{1}{2}".format(color,message,bcolors.ENDC))

if __name__ == '__main__':
    main()
